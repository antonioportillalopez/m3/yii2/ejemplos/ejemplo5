DROP DATABASE IF EXISTS ejemplo5;
CREATE DATABASE ejemplo5;
USE ejemplo5;

CREATE TABLE productos(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(255),
  foto varchar(255),
  almacen int NOT NULL,
  fecha date
);

CREATE TABLE almacenes(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(255),
  direccion varchar(255)
);

ALTER TABLE productos ADD CONSTRAINT FK_almacenes_productos 
  FOREIGN KEY (almacen) REFERENCES almacenes (id);


INSERT INTO almacenes (nombre,direccion) VALUES 
('Pe�acastillo','Calle Ricardo L�pez Aranda, 20H 2B'),
('Santander','Juan de Herra, 13 4C'),
('Malia�o', 'Avenida de Parayas, 61A'),
('Santander2','Calle Maria Cristina 1F 4B')
;

INSERT INTO productos(nombre,foto,almacen,fecha) VALUES 
('Impresora HP',1,1,'2020/01/07'),
('Calculadora cientifica',2,1,'2020/01/04'),
('Movil iphone',3,1,'2020/01/04'),
('Silla oficina',4,2,'2020/02/04'),
('Grapadora',5,3,'2020/01/24'),
('Boli',6,4,'2020/01/14'),
('Fotocopiadora',7,3,'2019/12/14'),
('TPV',8,3,'2019/11/24'),
('TV',9,3,'2019/10/28'),
('Port�til',10,2,'2020/02/29'),
('Lavadora',11,2,'2019/05/29'),
('Tostador',12,2,'2020/07/29'),
('Secador',13,1,'2020/02/29'),
('Horno',14,2,'2018/01/29'),
('Tenedor',15,2,'2017/02/07'),
('Libro',16,3,'2020/02/19'),
('Pantalla',17,1,'2019/04/17')

;

SELECT * FROM almacenes;
SELECT * FROM productos;