<?php // vista para crear productos en el CRUB

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Almacenes;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;




/* @var $this yii\web\View */
/* @var $model app\models\Productos */
/* @var $form yii\widgets\ActiveForm */
 $almacenes= Almacenes::find()->all();
 $datos=ArrayHelper::map($almacenes,'id','nombre');

?>
    

<div class="productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <!-- información de fecha https://www.yiiframework.com/doc/guide/2.0/en/input-file-upload -->
    
    <!-- imagenes -->
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'foto')->fileInput() ?>

    <button>Submit</button>

    <?php ActiveForm::end() ?>
    <!-- Fin imagenes -->
    
        
    <?php // echo $form->field($model, 'foto')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'almacen')->dropDownList($datos,
            ['prompt'=>'']) ?>

   <?php 
         // formato de entrada Datapickerd
        echo '<label>Fecha</label>';
        echo DatePicker::widget([
                                    'model' => $model, // por modelo
                                    'attribute'=>'fecha',
                                    'language'=>'es',
                                    //'value' => date('d-M-Y', strtotime('+2 days')), // por valor manual
                                    'options' => ['placeholder' => 'Introduzca la fecha'],
                                    'pluginOptions' => [
                                            'todayHighlight'=>true,
                                            'todayBtn'=>true,
                                            'format' => 'dd/mm/yyyy',
                                            'todayHighlight' => true,
                                            'autoclose'=>true,
                                    ]
                            ]);
     ?>
    
    <?php //echo $form->field($model, 'fecha')->textInput() // formato de entrada manual ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
